#!/bin/sh 

set -e

# called by uscan with '--upstream-version' <version> <file>
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
version=$2
tarball=$3

unzip $tarball
rm -rf META-INF

TAR=../${package}_${version}.orig.tar.gz
DIR=${package}-${version}.orig

mkdir -p $DIR/src/main/java
mv org/* $DIR/src/main/java
rmdir org

GZIP=--best tar --numeric --group 0 --owner 0 -c -v -z -f $TAR $DIR

rm -rf $tarball $DIR
